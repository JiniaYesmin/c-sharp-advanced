﻿
var A = new AccessModifier();
var C = new Construct();
var I = new Inherit();
var I2 = new Inherit2();
A.Print();
C.result("Math", 92);
var C1 = new Construct(20);
I.Intro("Araf", 6);
I2.NameMatch("Jamil");
Font T = new Text();
T.fontName = "Calibri";
T.fontSize = 16;

Text T1 = T as Text;
if (T1!=null)
{
    T1.textColor = "Green";
    T1.Meth();
}
else
{
    Console.WriteLine("Error!!");
}
var O = new Overload();
O.Method();
O.Method(12);
O.Method(13,14);
I.result("Physics", 84);
var abstract1 = new AbstractInherit();
abstract1.StartPoint = 10;
abstract1.EndPoint = 43;
abstract1.Distance();
abstract1.AbstractMethod();

IDrawable Interface;
Interface= new Triangle();
Interface.Draw();
Interface = new Circle();
Interface.Draw();