﻿public abstract class AbstractClass
{
    public abstract void AbstractMethod();
    public int StartPoint { get; set; }
    public int EndPoint { get; set; }
    public void Distance()
    {
        Console.WriteLine("Distance: "+(EndPoint-StartPoint));
    }

}