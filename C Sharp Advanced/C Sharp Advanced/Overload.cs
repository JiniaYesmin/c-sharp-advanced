﻿

class Overload
{
    public void Method()
    {
        Console.WriteLine("Hello!");
    }
    public void Method(int x)
    {
        Console.WriteLine(x + 5);
    }
    public void Method(int x, int y)
    {
        Console.WriteLine(x * y);
    }
}