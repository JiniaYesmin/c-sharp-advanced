﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks.Sources;

class Construct :AccessModifier
{
    public string name;
    public int age;
    public virtual void result(String Sub, int Scor)
    {
        this.subject = Sub;
        this.score = Scor;
        Console.WriteLine("Subject: "+ this.subject+"\tGrade: "+ this.Grade(score));
    }
    public Construct()

    {
        Console.WriteLine("This is the default Constructor");
    }
    public Construct(int x)
    {
        Console.WriteLine("parameterized constructor, contains integer: "+x);
    }
    static Construct()
    {
        Console.WriteLine("This is the static Constructor");
    }

}