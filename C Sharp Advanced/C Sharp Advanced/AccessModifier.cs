﻿using System;
using System.Security.Cryptography.X509Certificates;

class AccessModifier
{
    public string subject { get; set; }
    public int score { get; set; }
    private string S= "C Sharp";
    protected char Grade(int Score)
    {
        if(Score < 60)
        {
            return 'F';
        }
        else if(Score>=60 && Score <70)
        {
            return 'D';
        }
        else if (Score >= 70 && Score < 80)
        {
            return 'C';
        }
        else if (Score >= 80 && Score < 90)
        {
            return 'B';
        }
        else 
        {
           return 'A';
        }
    } 
    public void Print()
    {
        Console.WriteLine(S);
    }
}